package io.forfun.lizahelper

import java.time.ZonedDateTime

data class WorkbookBrief(val slug: String, val uploaded: ZonedDateTime, val issueCount: Int, val enriched: String)
data class Workbook(val slug: String, val uploaded: ZonedDateTime, val issues: List<Issue>)
data class EnrichedWorkbook(val slug: String, val enriched: Boolean, val uploaded: ZonedDateTime, val issues: List<IssueWithCoordinates>)
data class Issue(val attributes: Map<String, String>, val address: String)
data class Coordinates(val lon: Double, val lat: Double)
data class IssueWithCoordinates(val issue: Issue, val coordinates: Coordinates)


