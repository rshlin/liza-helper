package io.forfun.lizahelper.event

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct
import kotlin.math.max
import kotlin.reflect.full.isSubtypeOf
import kotlin.reflect.full.starProjectedType

@Configuration
class EventChannelConfiguration {

    //    todo
    @Bean
    fun domainEventChannel(): Channel<DomainEvent> = Channel(Channel.Factory.BUFFERED)
}

private val log = LoggerFactory.getLogger(EventListenerConfiguration::class.java)

@Configuration
class EventListenerConfiguration(
        listeners: List<EventListener<*>>,
        private val eventChannel: Channel<DomainEvent>
) {
    private val listeners = listeners.groupBy { listener ->
        listener::class.supertypes.first { it.isSubtypeOf(EventListener::class.starProjectedType) }.arguments[0].type
    }


    @PostConstruct
    fun init() {
        GlobalScope.launch {
            val threadsNumber = max(Runtime.getRuntime().availableProcessors(), 8)

            repeat(threadsNumber) {
                launch {
                    handleEvents()
                }
            }
        }
    }

    private suspend fun handleEvents() {
        for (event in eventChannel) {
            listeners[event::class.starProjectedType]?.forEach {
                try {
                    @Suppress("UNCHECKED_CAST")
                    (it as EventListener<Any>).handleEvent(event)
                } catch (ex: Exception) {
                    log.error("Unexpected exception occurred invoking listener: ${it::class}, with event: $event", ex)
                }
            }
        }
    }
}
