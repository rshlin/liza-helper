package io.forfun.lizahelper.event

import io.forfun.lizahelper.Workbook

sealed class DomainEvent
class WorkbookSaved(val workbook: Workbook): DomainEvent()
