package io.forfun.lizahelper.event

interface EventListener<in Event> {
    suspend fun handleEvent(event: Event)
}
