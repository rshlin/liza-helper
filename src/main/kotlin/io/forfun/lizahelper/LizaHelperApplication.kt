package io.forfun.lizahelper

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LizaHelperApplication

fun main(args: Array<String>) {
	runApplication<LizaHelperApplication>(*args)
}
