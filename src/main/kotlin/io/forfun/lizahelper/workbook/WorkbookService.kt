package io.forfun.lizahelper.workbook

import io.forfun.lizahelper.EnrichedWorkbook
import io.forfun.lizahelper.Workbook
import io.forfun.lizahelper.WorkbookBrief
import io.forfun.lizahelper.address.*
import io.forfun.lizahelper.event.DomainEvent
import io.forfun.lizahelper.event.WorkbookSaved
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.flow.*
import org.apache.poi.ss.usermodel.Row
import org.springframework.stereotype.Component
import java.time.ZonedDateTime

@Component
class WorkbookService(
        private val eventChannel: SendChannel<DomainEvent>
) {
    suspend fun saveWorkBook(slug: String, wb: org.apache.poi.ss.usermodel.Workbook): Workbook {
        val firstSheet = wb.firstSheet()
        val columnTitleRow = firstSheet.first(Row::containsAddressTitle)
        val cellTitles = columnTitleRow.getCellTitles()
        val addressColumnIndex = columnTitleRow.getAddressColumnIndex()

        return firstSheet
                .asFlow()
                .drop(columnTitleRow.rowNumber())
                .map { with(IssueExtractor) { it.getIssue(cellTitles, addressColumnIndex) } }.toList()
                .let { Workbook(slug, ZonedDateTime.now(), it) }
                .also { AddressCache.saveWorkbook(it) }
                .also { eventChannel.send(WorkbookSaved(it)) }
    }

    suspend fun listWorkBooks(): Flow<WorkbookBrief> = AddressCache.getWorkbooks().asFlow()

    suspend fun getBySlug(slug: String): EnrichedWorkbook? = AddressCache.findWorkbookBySlug(slug)
    suspend fun deleteBySlug(slug: String) = AddressCache.deleteWorkbook(slug)
}
