package io.forfun.lizahelper.workbook

import com.github.slugify.Slugify
import io.forfun.lizahelper.EnrichedWorkbook
import io.forfun.lizahelper.WorkbookBrief
import io.forfun.lizahelper.web.ResourceNotFoundException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.reactive.asFlow
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.http.codec.multipart.FilePart
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.io.ByteArrayInputStream
import java.io.File
import javax.annotation.PostConstruct

val slugify = Slugify()

@RestController
@RequestMapping("/workbooks")
class WorkbookController(private val workbookService: WorkbookService) {
    private lateinit var tmpDir: File

    @PostConstruct
    fun init() {
        tmpDir = createTempDir("uploads")
    }


    @FlowPreview
    @PostMapping(consumes = ["multipart/form-data"], produces = ["application/json"])
    suspend fun uploadWorkbook(@RequestPart("file") file: Mono<FilePart>): io.forfun.lizahelper.Workbook =
            file.asFlow()
                    .map {
                        val workbookSlug = slugify.slugify(it.filename())
                        val tmp = createTempFile(prefix = workbookSlug, directory = tmpDir)
                        it.transferTo(tmp)
                                .asFlow()
                                .flowOn(Dispatchers.IO)
                                .let {
                                    @Suppress("BlockingMethodInNonBlockingContext")
                                    workbookSlug to WorkbookFactory.create(tmp).also { tmp.delete() }
                                }
                    }
                    .single()
                    .let { (slug, workbook) ->
                        workbookService.saveWorkBook(slug, workbook)
                    }

    @GetMapping
    suspend fun listWorkbooks(): Flow<WorkbookBrief> = workbookService.listWorkBooks()

    @GetMapping(path = ["/{workbookSlug}"])
    suspend fun getWorkbookDetails(@PathVariable("workbookSlug") slug: String): EnrichedWorkbook =
            workbookService.getBySlug(slug) ?: throw ResourceNotFoundException()


    @DeleteMapping(path = ["/{workbookSlug}"])
    suspend fun deleteWorkbook(@PathVariable("workbookSlug") slug: String) =
            workbookService.deleteBySlug(slug)


/*
    @GetMapping(path = ["jobs/{jobId}"])
    suspend fun pollJobResult(@PathVariable("jobId") jobId: UUID): Map<String, Any?> {
        val (state, result) = addressService.pollJobResult(jobId)
        if (state == JobState.FAILED) {
            throw RuntimeException()
        }

        return mapOf(
                "state" to state,
                "result" to result
        )
    }
*/

}

fun ByteArray.toWorkBook(): Workbook = WorkbookFactory.create(ByteArrayInputStream(this))
