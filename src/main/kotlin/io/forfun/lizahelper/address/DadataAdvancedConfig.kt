package io.forfun.lizahelper.address

import org.springframework.boot.context.properties.ConfigurationProperties
import java.time.Duration

@ConfigurationProperties(prefix = "dadata")
class DadataAdvancedConfig {
    var parallelCapacity: Int = 2
    var backoffMaxAttempts: Long = 5
    var backoffDuration: Duration = Duration.ofMillis(500)
}
