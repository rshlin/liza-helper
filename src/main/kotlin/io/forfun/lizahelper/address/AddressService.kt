package io.forfun.lizahelper.address

import com.kuliginstepan.dadata.client.DadataClient
import com.kuliginstepan.dadata.client.domain.address.Address
import com.kuliginstepan.dadata.client.domain.address.AddressRequest
import com.kuliginstepan.dadata.client.domain.address.AddressRequestBuilder
import com.kuliginstepan.dadata.client.domain.address.FilterProperty
import io.forfun.lizahelper.Coordinates
import io.forfun.lizahelper.Issue
import io.forfun.lizahelper.IssueWithCoordinates
import io.forfun.lizahelper.Workbook
import io.forfun.lizahelper.event.EventListener
import io.forfun.lizahelper.event.WorkbookSaved
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.DataFormatter
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import reactor.util.retry.Retry
import java.util.*

private val log = LoggerFactory.getLogger(AddressService::class.java)


enum class JobState(val code: Byte) {
    COMPLETED(0), IN_PROGRESS(1), FAILED(2)
}

fun jobStateFromCode(code: Byte) = JobState.values().first { it.code == code }

@Component
class AddressService(
        private val dadataClient: DadataClient,
        private val dadataConfig: DadataAdvancedConfig
) : EventListener<WorkbookSaved> {

    @ExperimentalCoroutinesApi
    override suspend fun handleEvent(event: WorkbookSaved) {
        startAddressEnrichment(event.workbook)
    }

    @ExperimentalCoroutinesApi
    suspend fun startAddressEnrichment(workbook: Workbook): UUID {
        val ioScope = CoroutineScope(Dispatchers.IO)

        val jobId = UUID.randomUUID()
        workbook.issues
                .asFlow()
                .map { issue ->
                    ioScope.async {
                        val cachedCoordinates = AddressCache.getAddressCoordinates(issue.address)
                        if (cachedCoordinates == null) {
                            dadataClient.suggestAddress(issue.address.toMoscowAddressRequest())
                                    .retryWhen(Retry.backoff(dadataConfig.backoffMaxAttempts, dadataConfig.backoffDuration))
                                    .blockFirst()?.data?.getCoordinates().also {
                                        log.info("requested coordinates for address {}, result: {}", issue.address, it)
                                        if (it != null) {
                                            try {
                                                AddressCache.setAddressCoordinates(issue.address, it)
                                            } catch (e: Exception) {
                                                log.error("ошибка при сохранении координат: {}", it)
                                            }
                                        }
                                    }
                        }
                        Unit
                    }
                }
                .buffer(dadataConfig.parallelCapacity)
                .onEach { it.await() }
                .onCompletion { t ->
                    val jobState = if (t != null) {
                        log.error("ошибка при обработке запроса с идентификатором {}\nошибка: {}", jobId, t.message)
                        JobState.FAILED
                    } else {
                        log.info("задача {} завершена", jobId)
                        JobState.COMPLETED
                    }
                    AddressCache.saveJobState(jobId, jobState)
                }
                .launchIn(GlobalScope)

        AddressCache.saveJobState(jobId, JobState.IN_PROGRESS, workbook.slug)
        log.info("создана задача на обработку книги {}, номер задачи {}", workbook.slug, jobId)

        return jobId
    }

    suspend fun pollJobResult(jobId: UUID): Pair<JobState?, List<IssueWithCoordinates>?> {
        val state = AddressCache.getJobState(jobId)
        val result = if (state == JobState.COMPLETED) {
            AddressCache.getIssuesWithCoordinates(jobId)
        } else null

        return state to result
    }
}

fun String.toMoscowAddressRequest(): AddressRequest = AddressRequestBuilder.create(this).location(FilterProperty.REGION, "Москва").build()
fun Address.getCoordinates(): Coordinates = Coordinates(geoLon, geoLat)


fun org.apache.poi.ss.usermodel.Workbook.firstSheet(): Sheet = first()

private val addressKey = "Адрес"

fun Row.containsAddressTitle() = firstOrNull { it.getStringValue().contains(addressKey) } != null
fun Row.getAddressColumnIndex() = first { it.getStringValue().contains(addressKey) }.columnIndex
fun Row.getCellTitles() = map {
    val string = it.getStringValue()
    if (string.isEmpty()) {
        null
    } else {
        string
    }
}.toTypedArray()

fun Row.rowNumber() = rowNum + 1

fun Row.getCellValue(columnIndex: Int): String = getCell(columnIndex).getStringValue()
val cellFormatter = DataFormatter()
fun Cell.getStringValue(): String = cellFormatter.formatCellValue(this)

object IssueExtractor {
    fun Row.getIssue(cellTitles: Array<String?>, addressColumnIndex: Int) = take(cellTitles.size)
            .mapIndexed { idx, cell ->
                val title = cellTitles[idx]
                if (title == null) {
                    null
                } else {
                    title to cell.getStringValue()
                }
            }
            .filterNotNull()
            .toMap()
            .let { Issue(it, getCellValue(addressColumnIndex)) }
}

