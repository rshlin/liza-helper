package io.forfun.lizahelper.address

import io.forfun.lizahelper.*
import jetbrains.exodus.database.TransientEntityStore
import jetbrains.exodus.entitystore.Entity
import kotlinx.dnq.*
import kotlinx.dnq.query.*
import kotlinx.dnq.store.container.StaticStoreContainer
import kotlinx.dnq.util.initMetaData
import java.io.File
import java.time.Instant
import java.time.ZonedDateTime
import java.util.*
import kotlin.reflect.KProperty1

class XdCoordinates(entity: Entity) : XdEntity(entity) {
    companion object : XdNaturalEntityType<XdCoordinates>() {
        override val compositeIndices: List<List<KProperty1<XdCoordinates, *>>> = listOf(
                listOf(XdCoordinates::address, XdCoordinates::lon, XdCoordinates::lat)
        )

    }

    var address: String by xdRequiredStringProp<XdCoordinates>(unique = true)
    var lon: Double by xdRequiredDoubleProp<XdCoordinates>(unique = false)
    var lat: Double by xdRequiredDoubleProp<XdCoordinates>(unique = false)
}

class XdWorkBook(entity: Entity) : XdEntity(entity) {
    companion object : XdNaturalEntityType<XdWorkBook>()

    var slug by xdRequiredStringProp<XdWorkBook>(trimmed = true, unique = true)
    var uploaded by xdRequiredLongProp<XdWorkBook>()
    val issues by xdChildren0_N(XdIssue::workbook)
    val jobs by xdChildren0_N(XdJob::workbook)
}

class XdIssue(entity: Entity) : XdEntity(entity) {
    companion object : XdNaturalEntityType<XdIssue>()

    var workbook: XdWorkBook by xdParent(XdWorkBook::issues)
    val attributes by xdChildren0_N(XdIssueAttribute::issue)
    var address: String by xdRequiredStringProp<XdIssue>()
}

class XdIssueAttribute(entity: Entity) : XdEntity(entity) {
    companion object : XdNaturalEntityType<XdIssueAttribute>()

    var issue: XdIssue by xdParent(XdIssue::attributes)
    var key by xdRequiredStringProp<XdIssueAttribute>()
    var value by xdRequiredStringProp<XdIssueAttribute>()
}

class XdJob(entity: Entity) : XdEntity(entity) {
    companion object : XdNaturalEntityType<XdJob>()

    var jobId by xdRequiredStringProp<XdJob>(unique = true)
    var jobState by xdRequiredByteProp<XdJob>()
    var workbook: XdWorkBook by xdParent(XdWorkBook::jobs)
}

fun initXodus(): TransientEntityStore {
    XdModel.registerNodes(
            XdWorkBook,
            XdIssue,
            XdIssueAttribute,
            XdCoordinates,
            XdJob
    )

    val dbHome = File(System.getProperty("user.home"), ".liza-helper/db")
    val store = StaticStoreContainer.init(
            dbFolder = dbHome,
            environmentName = "db"
    )

    initMetaData(XdModel.hierarchy, store)

    return store
}

val store = initXodus()

fun Long.toZoneDateTime(): ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(this), TimeZone.getDefault().toZoneId())
fun XdMutableQuery<XdJob>.findLatestAssignedJob() = sortedBy(XdJob::entityId, false).firstOrNull()

object AddressCache {
    fun findWorkbookBySlug(slug: String) = store.transactional(readonly = true) {
        val workbook = XdWorkBook.query(XdWorkBook::slug eq slug.trim()).firstOrNull()

        workbook?.toEnrichedWorkbook(
                workbook.issues.toList().getWithCoordinates(),
                workbook.jobs.findLatestAssignedJob()?.jobState == JobState.COMPLETED.code
        )
    }

    fun getWorkbooks() = store.transactional(readonly = true) {
        XdWorkBook.all().asIterable()
                .map {
                    WorkbookBrief(
                            slug = it.slug,
                            uploaded = it.uploaded.toZoneDateTime(),
                            issueCount = it.issues.roughSize(),
                            enriched = it.jobs.findLatestAssignedJob()?.jobState?.let { jobStateFromCode(it).toString() }
                                    ?: "UNKNOWN"
                    )
                }
    }

    fun deleteWorkbook(slug: String) = store.transactional {
        XdWorkBook.query(XdWorkBook::slug eq slug).firstOrNull()?.delete()
    }

    fun getAddressCoordinates(address: String): Coordinates? = store.transactional(readonly = true) {
        XdCoordinates.query(XdCoordinates::address eq address.trim()).firstOrNull()?.toCoordinates()
    }

    fun setAddressCoordinates(address: String, coordinates: Coordinates): Coordinates =
            store.transactional {
                XdCoordinates.new {
                    this.address = address.trim()
                    lon = coordinates.lon
                    lat = coordinates.lat
                }.toCoordinates()
            }

    fun saveWorkbook(wb: Workbook): Workbook = store.transactional {
        XdWorkBook.query(XdWorkBook::slug eq wb.slug.trim()).firstOrNull() ?: XdWorkBook.new {
            slug = wb.slug
            uploaded = wb.uploaded.toInstant().toEpochMilli()
            wb.issues.forEach {
                issues.add(XdIssue.new {
                    address = it.address
                    it.attributes.forEach { (k, v) ->
                        attributes.add(XdIssueAttribute.new {
                            key = k
                            value = v
                        })
                    }
                })
            }
        }
        wb
    }


    fun getIssuesWithCoordinates(jobId: UUID): List<IssueWithCoordinates> =
            store.transactional(readonly = true) {
                val wb = XdJob.query(XdJob::jobId eq jobId.toString()).first().workbook
                wb.issues.toList().getWithCoordinates()
            }

    private fun List<XdIssue>.getWithCoordinates(): List<IssueWithCoordinates> = this
            .map { issue ->
                issue to XdCoordinates.query(XdCoordinates::address eq issue.address).firstOrNull()
            }
            .mapNotNull { (iss, c) ->
                if (c == null)
                    null
                else
                    IssueWithCoordinates(
                            issue = Issue(
                                    attributes = iss.attributes.toList().map { it.key to it.value }.toMap(),
                                    address = iss.address
                            ),
                            coordinates = Coordinates(c.lon, c.lat)
                    )
            }

    fun saveJobState(jobId: UUID, state: JobState, wbSlug: String? = null) {
        store.transactional {
            when (val job = XdJob.query(XdJob::jobId eq jobId.toString()).firstOrNull()) {
                null -> XdJob.new {
                    this.jobId = jobId.toString()
                    this.jobState = state.code
                    if (wbSlug != null) {
                        val workbook = XdWorkBook.query(XdWorkBook::slug eq wbSlug).single()
                        this.workbook = workbook
                    }
                }
                else -> job.jobState = state.code
            }
        }
    }

    fun getJobState(jobId: UUID): JobState = store.transactional {
        XdJob.query(XdJob::jobId eq jobId.toString()).first().jobState.let { jobStateFromCode(it) }
    }
}

fun XdCoordinates.toCoordinates() = Coordinates(
        lat = lat,
        lon = lon
)

fun XdWorkBook.toEnrichedWorkbook(issues: List<IssueWithCoordinates>, enriched: Boolean) = EnrichedWorkbook(
        slug = slug,
        uploaded = uploaded.toZoneDateTime(),
        issues = issues,
        enriched = enriched
)
