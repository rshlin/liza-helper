package io.forfun.lizahelper.web

import io.forfun.lizahelper.address.DadataAdvancedConfig
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.web.reactive.function.server.RouterFunctions.resources
import org.springframework.web.reactive.function.server.router
import java.time.Duration

@Configuration
@EnableConfigurationProperties(DadataAdvancedConfig::class)
class WebConfiguration {
    @Bean
    fun resRouter() = resources("/**", ClassPathResource("static/"))

    @Bean
    fun indexRouter(@Value("classpath:/static/index.html") html: Resource) =
            router {
                GET("/") {
                    ok().contentType(TEXT_HTML).bodyValue(html).cache(Duration.ofSeconds(1))
                }
            }
}
